package com.example.ribraort

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var runnable: Runnable
    private var handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Let's  create a new MediaPlayer object
        val mediaplayer: MediaPlayer = MediaPlayer.create(this, R.raw.music)
        //Now let's add seekbar functionalities
        seekbar.progress = 0
        //add now we will add the maximum value of our seekbar the duration of the music
        seekbar.max = mediaplayer.duration
        //now let's  create our play  button event
        play_btn.setOnClickListener {

            //first let's check that  the media player  is not playing
            if (!mediaplayer.isPlaying) {
                mediaplayer.start()
                //and we will change  the button's image
                play_btn.setImageResource(R.drawable.ic_baseline_pause_24)
            } else {
                mediaplayer.pause()
                play_btn.setImageResource(R.drawable.ic_baseline_play_arrow_24)
                //Let's see it now

            }
        }
//Now we will add the seek bar event
        //when we change our seek bar progress the song will change
        seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, pos: Int, changed: Boolean) {
                if (changed) {
                    mediaplayer.seekTo(pos)
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })
        runnable = Runnable {
            seekbar.progress = mediaplayer.currentPosition
            handler.postDelayed(runnable, 1000)

        }
        handler.postDelayed(runnable, 1000)

        mediaplayer.setOnCompletionListener {
            play_btn.setImageResource(R.drawable.ic_baseline_play_arrow_24)
            seekbar.progress = 0
        }
    }

}
